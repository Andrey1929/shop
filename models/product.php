<?php

// Характеристика товара.
  class Product {
    public $id;
    public $price;
    public $name;
    public $description;
    public $picture;
    private $connection;

    public function getConnection(){
        $connection = new mysqli('localhost', 'root', '','shop');
        if (!$connection) {
           die('Ошибка соединения: ' . mysqli_error());
        }
        return $connection;
    }

    public function create($name,$price,$description,$picture){
        $connection = $this->getConnection();
        $sql = "INSERT INTO `product` (`name`, `price`, `description`, `picture`) VALUES ('$name', '$price', '$description', '$picture')";
            if (mysqli_query($connection, $sql)) {
               // echo "New record created successfully";
            } else {
              echo "Error: " . $sql . "" . mysqli_error($connection);
              die();
            }
            // $conn->close();
    }


   public function delete($id){
       $connection = $this->getConnection();
       $sql = "DELETE FROM product WHERE id = $id";
           if (mysqli_query($connection, $sql)) {
              // echo "New record created successfully";
           } else {
             echo "Error: " . $sql . "" . mysqli_error($connection);
             die();
           }
   }



    public function update($id,$name,$price,$description,$picture){
        $connection = $this->getConnection();
        $sql = "UPDATE product SET name = '$name', price = '$price', description = '$description', picture = '$picture' WHERE id = $id LIMIT 1";
            if (mysqli_query($connection, $sql)) {
               // echo "New record created successfully";
            } else {
              echo "Error: " . $sql . "" . mysqli_error($connection);
              die();
            }

    }

    public function getById($id)  {
        $connection = $this->getConnection();
        $sql = "SELECT id, name, price, description, picture FROM product WHERE id = $id";
        $result = $connection->query($sql);

        if ($id>0&&$result->num_rows > 0) {
          // output data of each row
          while($row = $result->fetch_assoc()) {
            // echo "id: " . $row["id"]. " - Name: " . $row["name"]. " " . $row["lastname"]. "<br>";
            $this->id = $row["id"]??"";
            $this->name =$row["name"]??"";
            $this->price = $row["price"]??"";
            $this->description = $row["description"]??"";
            $this->picture = $row["picture"]??"";
            break;
          }
        }
         return $this;

    }

    public function getAll(){
        $connection = $this->getConnection();
        $sql = "SELECT * FROM product";
        $result = $connection->query($sql);
        $array = [];
        if ($result->num_rows > 0) {
          // output data of each row
          while($row = $result->fetch_assoc()) {
            $product = new Product();
            $product->id = $row["id"]??"";
            $product->name =$row["name"]??"";
            $product->price = $row["price"]??"";
            $product->description = $row["description"]??"";
            $product->picture = $row["picture"]??"";
            $array[] = $product;
          }
        }
         return $array;
    }
}
