<?php
public function fcreate($sum,$status,$created){
    $content = file_get_contents(__DIR__ . "/../data/order.csv");
    $content .= $this->getNextId() . ';' . $sum . ';' . $status . ';'. $created .';' . date('Y-m-d H:i:s', time()) .':'. $updated .  PHP_EOL;

    file_put_contents(__DIR__ . "/../data/order.csv", $content);
}
public function delete($id){
    $content = "";
    $data = file_get_contents(__DIR__ . "/../data/order.csv");
    $array = str_getcsv ($data,"\n");
    foreach($array as $line){
      $filds = str_getcsv($line,";");
      if($id != $filds[0]){
        $content .= $filds[0] . ';' . $filds[1] . ';' . $filds[2] . ';' . $filds[3] . ';' . $filds[4]  . PHP_EOL;

      }
      print_r($content);
      file_put_contents(__DIR__ . "/../data/order.csv",$content);
}
public function update($id,$sum,$status,$created){
     $content = "";
     $data = file_get_contents(__DIR__ . "/../data/order.csv");
     $array = str_getcsv ($data,"\n");
     foreach($array as $line){
       $filds = str_getcsv($line,";");
       if($id == $filds[0]){
           $content .=  $filds[0] . ';' . $sum . ';' . $status . ';'. $created .';' . date('Y-m-d H:i:s', time()). PHP_EOL;
       }
       else{
         $content .= $filds[0] . ';' . $filds[1] . ';' . $filds[2] .';' . $filds[3] . ';' . $filds[4] .  PHP_EOL;
       }
     }
     print_r($content);
     file_put_contents(__DIR__ . "/../data/order.csv",$content);

 }
 public function getNextId(){
     $maxID  = 0;
     $data = file_get_contents(__DIR__ . "/../data/order.csv");
     $array = str_getcsv ($data,"\n");
     foreach($array as $line){
       $filds = str_getcsv($line,";");
       if($maxID < $filds[0]){
         $maxID = $filds[0];
       }
     }
      return $maxID+1;
 }

 public function getById($id)  {
     $data = file_get_contents(__DIR__ . "/../data/order.csv");
     $array = str_getcsv ($data,"\n");
     foreach($array as $line){
       $filds = str_getcsv($line,";");
       if($id == $filds[0]){
         $this->id = $filds[0]??"";
         $this->sum = $filds[1]??"";
         $this->status = $filds[2]??"";
         $this->created = $filds[3]??"";
         $this->updated = $filds[4]??"";
       }
     }

     return $this;
 }


   public function getAll(){
       $data = file_get_contents(__DIR__ . "/../data/order.csv");
       $array = str_getcsv ($data,"\n");
       $result = array();
       foreach($array as $line){
         $filds = str_getcsv($line,";");
         $order = new Order();
         $order->id = $filds[0]??"";
         $order->sum = $filds[1]??"";
         $order->status = $filds[2]??"";
         $order->created = $filds[3]??"";
         $order->updated = $filds[4]??"";
         $result[] = $order;

       }
      return $result;

   }
