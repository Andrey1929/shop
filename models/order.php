<?php

// Заказ товара
class Order {
    const STATUS_NEW = 0;
    const STATUS_PROCESING = 10;
    const STATUS_COMPLITED = 20;
    const STATUS_CANSELED = 30;

  // public $products = array();
  public $id;
  public $sum;
  public $status;
  public $created;
  public $updated;
  private $connection;

  public function getConnection(){
      $connection = new mysqli('localhost', 'root', '','shop');
      if (!$connection) {
         die('Ошибка соединения: ' . mysqli_error());
      }
      return $connection;
  }

  public function create($sum,$status,$created,$updated){
      $connection = $this->getConnection();
      $sql = "INSERT INTO `order` (`sum`, `status`, `created`, `updated`) VALUES ('$sum', '$status', '$created', '$updated')";
          if (mysqli_query($connection, $sql)) {
             // echo "New record created successfully";
          } else {
            echo "Error: " . $sql . "" . mysqli_error($connection);
            die();
          }
          // $conn->close();
  }

  public function delete($id){
      $connection = $this->getConnection();
      $sql = "DELETE FROM `order` WHERE id = $id";
          if (mysqli_query($connection, $sql)) {
             // echo "New record created successfully";
          } else {
            echo "Error: " . $sql . "" . mysqli_error($connection);
            die();
          }
  }

  public function update($id,$sum,$status,$created,$updated){
      $connection = $this->getConnection();
      $sql = "UPDATE `order` SET sum = '$sum', status = '$status', created = '$created', updated = '$updated' WHERE id = $id LIMIT 1";
          if (mysqli_query($connection, $sql)) {
             // echo "New record created successfully";
          } else {
            echo "Error: " . $sql . "" . mysqli_error($connection);
            die();
          }

  }

  public function getById($id)  {
      $connection = $this->getConnection();
      $sql = "SELECT id, sum, status, created, updated FROM `order` WHERE id = $id";
      $result = $connection->query($sql);
      if (is_object($result)&&$result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
          // echo "id: " . $row["id"]. " - Name: " . $row["name"]. " " . $row["lastname"]. "<br>";
          $this->id = $row["id"]??"";
          $this->sum =$row["sum"]??"";
          $this->status = $row["status"]??"";
          $this->created = $row["created"]??"";
          $this->updated = $row["updated"]??"";
          break;
        }
      }
       return $this;

  }

  public function getAll(){
      $connection = $this->getConnection();
      $sql = "SELECT * FROM `order`";
      $result = $connection->query($sql);
      $array = [];
      if (is_object($result)&&$result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
          $order = new Order();
          $order->id = $row["id"]??"";
          $order->sum =$row["sum"]??"";
            $order->status = $row["status"]??"";
          $order->created = $row["created"]??"";
          $order->updated = $row["updated"]??"";
          $array[] = $order;
        }
      }
       return $array;
  }

public function getActiveOrder(){
  $connection = $this->getConnection();
  $sql = "SELECT * FROM `order` WHERE status = 1";
  $result = $connection->query($sql);
  if (is_object($result)&&$result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $order = new Order();
      $order->id = $row["id"]??"";
      $order->sum =$row["sum"]??"";
      $order->status = $row["status"]??"";
      $order->created = $row["created"]??"";
      $order->updated = $row["updated"]??"";
      return $order;
    }
  }
   return new Order();

}

   public function addProduct($id){
     $connection = $this->getConnection();
     $order = $this->getActiveOrder();
     $orderId = $order->id;
     $sql = "INSERT INTO `order_product` (`order_id`, `product_id`, `quantity`) VALUES ('$orderId', '$id', '1')";
         if (mysqli_query($connection, $sql)) {
            // echo "New record created successfully";
         } else {
           echo "Error: " . $sql . "" . mysqli_error($connection);
           die();
         }

   }

}
