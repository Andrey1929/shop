<?php
include_once(__DiR__ . "/../controllers/ProductController.php");
include_once(__DiR__ . "/../controllers/OrderController.php");

$productController = new ProductController();
$orderController = new OrderController();

if(isset($_GET["action"])&&$_GET["action"]=="buy"){
$orderController->addProduct(intval($_GET["id"]));
} else if(isset($_GET["id"])&&intval($_GET["id"])>0){
    $productController->read(intval($_GET["id"]));
}
else{
$productController->list();
}
