<?php
include_once(__DIR__ . "/../../models/product.php");

class ProductController {

    public function list(){
        $model = new Product();
        $products = $model->getAll();
        include(__DIR__ . "/../views/products/list.php");
    }

    public function read($id){
    $model = new Product();
    $product = $model->getById($id);
    include_once(__DIR__ . "/../views/products/once.php");
    }


}
