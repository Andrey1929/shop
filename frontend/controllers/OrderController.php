<?php
include_once(__DIR__ . "/../../models/order.php");

class OrderController {

    public function list(){
        $model = new Order();
        $orders = $model->getAll();
        include_once(__DIR__ . "/../views/order/list.php");
    }

    public function read($id){
      $model = new Order();
      $order = $model->getById($id);
      include_once(__DIR__ . "/../views/order/once.php");
    }
    public function addProduct($id){
        $order = new Order();
        $order->addProduct($id);
        header('Location: /oop/product/frontend/product/index.php?order');
    }

}
