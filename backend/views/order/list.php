<html>
<head>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <script
  src="https://code.jquery.com/jquery-3.5.0.js"
  integrity="sha256-r/AaFHrszJtwpe+tHyNi/XCfMxYpbsRg2Uqn0x3s2zc="
  crossorigin="anonymous"></script>
  <script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
  integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
  crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<style>
form {
    margin: 20px auto;
    width: 200px;
}
</style>
</head>
<body>

    <?php
    print "<a href='/oop/product/backend/order/index.php?id&order'class='btn btn-success'>Create</a> <br>";
    print "<table class='table table-striped table-dark'>";
    print "<tr><th>id</th><th>sum</th><th>status</th><th>created</th><th>updated</th><th></th></tr>";
    foreach ($orders as $o) {
       print "<tr><td>" . $o->id . "</td><td>" . $o->sum. "</td><td>" . $o->status . "</td><td>" . $o->created . "</td><td>" . $o->updated . "</td><td><a href='/oop/product/backend/order/index.php?order&id=".$o->id."' class='btn'><i class='fa fa-pencil ' aria-hidden='true'></i>edit</a><a href='/oop/product/backend/order/index.php?action=delete&order&id=".$o->id."' class='btn'><i class='fa fa-pencil ' aria-hidden='true'></i>delete</a></td></tr>";
    }
    print "</table>";
    ?>
    <script>
    $("td").resizable({
    handles: 's, e',
    resize: function (event, ui) {
        console.log(ui.size.height);
        $(this).siblings().height(ui.size.height);
    }
    });
    </script>
    </body>
    </html>
