<?php
include_once(__DIR__ . "/../../models/order.php");

class OrderController {

    public function list(){
        $model = new Order();
        $orders = $model->getAll();
        include(__DIR__ . "/../views/order/list.php");
    }

    public function form(){
        $model = new Order();
        $order = $model->getById($_GET['id']??0);
        include(__DIR__ . "/../views/order/form.php");
    }

    public function create(){
        $data = $_POST;
        $created = date("Y-m-d H:i:s",time());
        $updated = date("Y-m-d H:i:s",time());
        $order = new Order();
         if(intval($data["id"])>0){
          $order->update($data["id"],$data["sum"],$data["status"],$created,$updated);
        }
        else{
          $order->create($data["sum"],$data["status"],$created,$updated);
        }
        header('Location: /oop/product/backend/order/index.php?order');
      }

    public function delete($id){
        $order = new Order();
        $order->delete($id);
        header('Location: /oop/product/backend/order/index.php?order');
    }

  
}
