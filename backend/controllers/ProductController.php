<?php
include_once(__DIR__ . "/../../models/product.php");

class ProductController {

    public function list(){
        $model = new Product();
        $products = $model->getAll();
        include(__DIR__ . "/../views/products/list.php");
    }

    public function form(){
        $model = new Product();
        $product = $model->getById($_GET['id']??0);
        include(__DIR__ . "/../views/products/form.php");
    }

    public function create(){
        $uploaddir = 'C:\\Server\\data\\htdocs\\oop\\product\\files\\';
        $fileName = basename($_FILES['picture']['name']);
        $uploadfile = $uploaddir . $fileName;
        $data = $_POST;
        if (!empty($fileName)&&!move_uploaded_file($_FILES['picture']['tmp_name'], $uploadfile)) {
          echo "Файл не загружен\n";
          die();
        }
        $product = new Product();
        if(intval($data["id"])>0){
            $product->update($data["id"],$data["name"],$data["price"],$data["description"],$fileName);
        }
        else{
            $product->create($data["name"],$data["price"],$data["description"],$fileName);
        }
        header('Location: /oop/product/backend/product/index.php?product');
    }

    public function delete($id){
        $product = new Product();
        $product->delete($id);
        header('Location: /oop/product/backend/product/index.php?product');
    }
}
